<?php

return [
    'field_types' => [
        'text' => ['label' => 'Text'],
        'editorjs' => ['label' => 'Editeur de block', 'view_namespace' => 'kda-backpack-custom-fields::fields'],
        'checkbox' => ['label' => 'Case à cocher'],
        'browse' => ['label' => 'Fichier'],
       // 'button' => ['label' => 'Bouton','key'=>'button'],
        'select2_from_array' => ['label'=>'Liste'],
        /*  'imagecrop'=> ['label'=> 'Image croppable','view_namespace'=>'kda-backpack-custom-fields::fields','options'=>[
            'metadata_field'=>'meta',
            'original_field'=>'original',
            'ratio'=>1,
            'crop'=>true
        ]],*/
    ],
    'views'=> [
        'render'=>'sc::components.render',
        'render-block'=>'sc::components.render-block',
        'render-field'=>'sc::components.render-field',
        'field-editorjs'=>'sc::components.field-editorjs',
        'field-text'=>'sc::components.field-text',
        'field-browse'=>'sc::components.field-browse',
        'field-select'=>'sc::components.field-select',

        'ejs-block-paragraph'=>'sc::components.ejs.paragraph',
        'ejs-block-render'=>'sc::components.ejs.render',
        'ejs-block-image'=>'sc::components.ejs.image',
        'ejs-block-header'=>'sc::components.ejs.header',
        'ejs-block-list'=>'sc::components.ejs.list',
        'ejs-block-table'=>'sc::components.ejs.table',

    ],
    'root_templates'=> [
        'site'=>'Site'
    ],
    'default_root_template'=>'site',
    'enable_scheduled_content'=>false
];
