<?php

namespace KDA\Backpack\StructuredEditor\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use DB;

/**
 * Class StaticContentCrudController
 * @package KDA\Backpack\StructuredEditor\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */

class StaticPageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;
    use \KDA\Backpack\Subcontroller\Operations\ManageSubControllersOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;


    public function setup()
    {
        $this->crud->setModel('KDA\Backpack\StructuredEditor\Models\StaticPage');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/staticpage');
        $this->crud->setEntityNameStrings('page', 'pages');
        $this->loadPermissions(
            'page',
            ['list', 'preview', 'show', 'edit', 'create', 'update','content'],
            [
                'read' => [
                    'list',
                    'show',
                    'preview'
                ],
                'write' => [
                    'create',
                    'delete',
                    'update',
                    'edit'
                ],
                'content' => [
                    'content'
                ]


            ]
        );
        $this->setupManageSubControllersOperation();
    }



    protected function setupManageSubControllersOperation()
    {
        $this->setSubControllers([
            'staticpagecontent' => [
                'key' => 'page_id',
                'label' => 'Gérer le contenu',
                'operation_name'=> 'content'// this handles access to 
            ]
        ], 'staticpage');
    }


    protected function setupListOperation()
    {

        CRUD::addColumn([
            'name' => 'name'
        ]);
        CRUD::addColumn([
            'name' => 'template'
        ]);
        CRUD::addColumn([
            "name" => 'page',
            'type' => 'closure',
            'label' => 'Url',
            'function' => function ($entry) {
                return $entry->slugs->first()->getFullPath();
            },
            
            
        ]);
        CRUD::addColumn([
            'name' => 'blocks',
            'type' => 'relationship_count'
        ]);
    }

    protected function setupCreateOperation()
    {
        CRUD::addField([
            'name' => 'name',
        ]);
        CRUD::addField([
            'name' => 'template',
            'type' => 'select_from_array',
            'options' => config('kda.backpack.structured_editor.root_templates'),
            'defaultValue' => config('kda.backpack.structured_editor.default_root_template'),
            'default' => config('kda.backpack.structured_editor.default_root_template'),
        ]);
        CRUD::addField([
            'name' => 'meta_title',
            'tab'=> 'SEO'
        ]);
        CRUD::addField([
            'name' => 'meta_description',
            'tab'=> 'SEO'

        ]);
        CRUD::addField([
            'name' => 'meta_keywords',
            'tab'=> 'SEO'

        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
