<?php

namespace KDA\Backpack\StructuredEditor\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use DB;

/**
 * Class StaticContentCrudController
 * @package KDA\Backpack\StructuredEditor\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */

class ViewComposerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    

    public function setup()
    {
        $this->crud->setModel('KDA\Backpack\StructuredEditor\Models\ViewComposer');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/viewcomposer');
        $this->crud->setEntityNameStrings('viewcomposer', 'viewcomposers');
        $this->loadPermissions('viewcomposer');
    }





    protected function setupListOperation()
    {
        kda_no_warranty_alert();
        CRUD::addColumn([
            'name' => 'name'
        ]);
        CRUD::addColumn([
            'name' => 'description'
        ]);
    }

    protected function setupCreateOperation()
    {
        CRUD::addField([
            'name' => 'name',
        ]);
        CRUD::addField([
            'name' => 'description',
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
