<?php

namespace KDA\Backpack\StructuredEditor\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;
use KDA\Backpack\StructuredEditor\Models\StaticContent;

trait PreviewStructuredContentOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupPreviewStructuredContentRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/{id}/previewstructuredcontent', [
            'as'        => $routeName.'.previewstructuredcontent',
            'uses'      => $controller.'@previewstructuredcontent',
            'operation' => 'previewstructuredcontent',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupPreviewStructuredContentDefaults()
    {
        $this->crud->allowAccess('previewstructuredcontent');

        $this->crud->operation('previewstructuredcontent', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            // $this->crud->addButton('top', 'previewstructuredcontent', 'view', 'crud::buttons.previewstructuredcontent');
            $this->crud->addButton('line', 'previewstructuredcontent', 'view', 'sc::crud.buttons.previewstructuredcontent');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function previewstructuredcontent($id)
    {
        $this->crud->hasAccessOrFail('previewstructuredcontent');

        $block = StaticContent::find($id);

        // load the view
        return view("sc::crud.preview", ['block'=>$block]);
    }
}
