<?php

namespace KDA\Backpack\StructuredEditor\Http\Controllers\Admin;

use KDA\Backpack\StructuredEditor\Http\Requests\StaticContentRequest;
use KDA\Backpack\StructuredEditor\Http\Requests\StaticContentUpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use KDA\Backpack\StructuredEditor\Models\StaticContent;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use DB;
use KDA\Backpack\StructuredEditor\Facades\StructuredContent;
use Backpack\CRUD\app\Library\Widget;

/**
 * Class StaticContentCrudController
 * @package KDA\Backpack\StructuredEditor\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */

function is_associative_array(array $arr)
{
    if (array() === $arr) return false;
    return array_keys($arr) !== range(0, count($arr) - 1);
}
class StaticContentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    } //IMPORTANT HERE
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    } //IMPORTANT HERE
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use Operations\PreviewStructuredContentOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;
    use \KDA\Backpack\StructuredEditor\Http\Controllers\Traits\GenerateFields;

    public function setup()
    {
        $this->crud->setModel('KDA\Backpack\StructuredEditor\Models\StaticContent');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/staticcontent');
        $this->crud->setEntityNameStrings('contenu', 'Contenus');
        $this->loadPermissions('static_content');
    }


    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();
        $this->crud->query->addSelect(DB::raw('JSON_LENGTH(JSON_EXTRACT(value, "$")) as length'));
        CRUD::column('pages')->label('Pages');

        CRUD::column('contentKey')->attribute('hint')->searchLogic(function ($query, $column, $searchTerm) {
            $query->whereHas('contentKey', function ($q) use ($column, $searchTerm) {
                $q->where('name', 'like', '%' . $searchTerm . '%')
                    ->orWhere('hint', 'like', '%' . $searchTerm . '%');
            });
        });
        CRUD::addColumn([
            "name" => 'filled',
            'type' => 'closure',
            'label' => 'Rempli',
            'function' => function ($entry) {
                //$value = json_decode($entry->value, true);
                $value = $entry->value;
                if (is_array($value) && count($value) > 0) {
                    return is_associative_array($value) ? 'oui' : count($value);
                } else {
                    return '-';
                }
            },
            'orderable' => true,
            'orderLogic' => function ($query, $column, $columnDirection) {
                $query
                    // ->addSelect(DB::raw('JSON_LENGTH(JSON_EXTRACT(value, "$")) as length'))
                    ->orderBy('length', $columnDirection);
            },
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if (intval($entry->length) > 0) {
                        return 'badge badge-success';
                    }
                    return 'badge badge-error';;
                },
            ],

        ]);
        //  CRUD::column('starting');
        //  CRUD::column('ending');
        CRUD::addColumn([
            'label' => 'Status',
            'type' => 'closure',
            'function' => function ($entry) {
                return '-';
            },
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->is_active) {
                        return 'badge badge-success';
                    } else if ($entry->is_planned) {
                        return 'badge badge-primary';
                    } else {
                        return 'badge badge-error';
                    }


                    return 'badge badge-default';
                },
            ],
            'name' => 'enabled'
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(StaticContentRequest::class);
        CRUD::removeAllSaveActions();
        $this->crud->addSaveAction([
            'name' => 'save_action_one',
            'redirect' => function ($crud, $request, $itemId) {
                return $crud->route . '/' . $itemId . '/edit';
            },
            'button_text' => trans('kda::create_and_edit'),
            'visible' => function ($crud) {
                return true;
            },
            'referrer_url' => function ($crud, $request, $itemId) {
                return $crud->route;
            },
            'order' => 1,
        ]);


        // TODO: remove setFromDb() and manually define Fields
        CRUD::addField([
            'type' => "relationship",
            'name' => 'contentKey', // the method on your model that defines the relationship
            'label' => 'Type de contenu', // the method on your model that defines the relationship
            'attribute' => 'hint',
            'ajax' => false,
            'wrapper' => ['class' => 'col-md-6'],

            // that way the assumed URL will be "/admin/tag/inline/create"
        ]);

        if (config('kda.backpack.structured_editor.enable_scheduled_content', false)) {
            CRUD::addField([
                'type'  => 'null_date_picker',
                'name' => 'start_on', // the method on your model that defines the relationship
                'label' => 'Début de validité', // the method on your model that defines the relationship
                'ajax' => false,
                'view_namespace' => 'kda-backpack-custom-fields::fields',
                'allows_null' => true,

                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd.mm.yyyy',
                    'language' => 'fr'
                ],
                'wrapper' => ['class' => 'col-md-6'],

                // that way the assumed URL will be "/admin/tag/inline/create"
            ]);
            CRUD::addField([
                'type'  => 'null_date_picker',
                'name' => 'end_on', // the method on your model that defines the relationship
                'label' => 'Fin de validité', // the method on your model that defines the relationship
                'ajax' => false,
                'view_namespace' => 'kda-backpack-custom-fields::fields',
                'allows_null' => true,

                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd.mm.yyyy',
                    'language' => 'fr'
                ],
                'wrapper' => ['class' => 'col-md-6'],

                // that way the assumed URL will be "/admin/tag/inline/create"
            ]);
            CRUD::addField([
                'type' => "toggle",
                'view_namespace' => 'kda-backpack-custom-fields::fields',

                'name' => 'enabled', // the method on your model that defines the relationship
                'label' => 'Actif', // the method on your model that defines the relationship
                'default' => true,
                'ajax' => false,
                'wrapper' => ['class' => 'col-md-6'],

                // that way the assumed URL will be "/admin/tag/inline/create"
            ]);
        }
    }

    protected function setupUpdateOperation()
    {
        $field_types = config('kda.backpack.structured_editor.field_types');




        $this->crud->setValidation(StaticContentUpdateRequest::class);
            // TODO: remove setFromDb() and manually define Fields

        ;


        CRUD::addField([
            'type' => "relationship",
            'name' => 'contentKey',
            'attribute' => 'hint',

            'label' => 'Contenu',
            'attributes' => [
                'disabled'    => 'disabled',
            ],
            'ajax' => false,
        ]);

        if (config('kda.backpack.structured_editor.enable_scheduled_content', false)) {
            CRUD::addField([
                'type'  => 'null_date_picker',
                'name' => 'start_on', // the method on your model that defines the relationship
                'view_namespace' => 'kda-backpack-custom-fields::fields',
                'allows_null' => true,
                'label' => 'Début de validité', // the method on your model that defines the relationship
                'ajax' => false,
                'hint' => 'si vide, toujours valide',

                'wrapper' => ['class' => 'col-md-4'],

                // that way the assumed URL will be "/admin/tag/inline/create"
            ]);
            CRUD::addField([
                'type'  => 'null_date_picker',
                'name' => 'end_on', // the method on your model that defines the relationship
                'view_namespace' => 'kda-backpack-custom-fields::fields',
                'allows_null' => true,

                'label' => 'Fin de validité', // the method on your model that defines the relationship
                'ajax' => false,
                'hint' => 'si vide, n\'expire jamais',

                'wrapper' => ['class' => 'col-md-4'],

                // that way the assumed URL will be "/admin/tag/inline/create"
            ]);
            CRUD::addField([
                'type' => "toggle-label",
                'view_namespace' => 'kda-backpack-custom-fields::fields',
                'name' => 'enabled', // the method on your model that defines the relationship
                'label' => 'Actif', // the method on your model that defines the relationship
                'default' => true,
                'ajax' => false,
                'wrapper' => ['class' => 'col-md-4'],
                // that way the assumed URL will be "/admin/tag/inline/create"
            ]);
        }

        $fields = [];
        $entry = $this->crud->getCurrentEntry();

        $contentType = $entry->contentKey->contentType;
        $repeatable = $contentType->repeatable;

        $is_repeatable = $repeatable['repeatable'] != "0";

        $fields = $this->generateFields($contentType);
        $slots = $this->generateSlots($entry->contentKey);

        if ($is_repeatable) {
            CRUD::addField([
                'type' => "repeatable",
                'name' => 'value',

                'label' => $entry->contentKey->hint,
                'tab' => 'Contenu',
                'fields' =>  $fields

                // that way the assumed URL will be "/admin/tag/inline/create"
            ]);
        } else {
            collect($fields)->map(function ($f) {
                $ns = $field_types[$f['type']]['view_namespace'] ?? '';
                if ($ns) {
                    $f['view_namespace'] = $ns;
                }

                $f['store_in'] = 'value';
                $f['fake'] = true;
                CRUD::addField($f);
            });
        }

        foreach ($slots as $slot) {
            foreach ($slot as $field) {
                $field['tab'] = '*' . $field['tab'];
                CRUD::addField($field);
            }
        }
        if (count($slots) > 0) {
            Widget::add([
                'type'         => 'alert',
                'class'        => 'alert alert-primary mb-2 col-sm-6',
                'heading'      => 'Contenu multiple',
                'content'      => "Attention, en éditant ce block vous pouvez modifier du contenu présent sur d'autres pages.
                <br/>
                Les onglets présentant une * peuvent être présent sur une autre partie du site.
                
                ",
                'close_button' => false, // show close button or not
            ]);
        }
    }


    public function store()
    {

        $response = $this->traitStore();


        return $response;
    }

    public function update()
    {

        $response = $this->traitUpdate();

        $slots = $this->crud->entry->contentKey->slots;

        foreach ($slots as $slot) {
            $slot_name = $slot->pivot->slot_name;
            if ($slot->contentType->is_repeatable) {

                $value = request()->input('slot_' . $slot_name);

                $slotContent = StructuredContent::getCurrentContentForKeyId($slot->id);

                $slotContent->value = json_decode($value);
                $slotContent->save();
            } else {
                $fields = collect($slot->contentType->fields)->pluck('name');
                $slotContent = StructuredContent::getCurrentContentForKeyId($slot->id);
                $value = $slotContent->value;
                foreach ($fields as $field) {
                    $value[$field] = request()->input($slot_name . '_' . $field);
                }
                $slotContent->value = $value;
                $slotContent->save();
            }
        }

        return $response;
    }
}
