<?php

namespace KDA\Backpack\StructuredEditor\Http\Controllers\Admin;

use KDA\Backpack\StructuredEditor\Http\Requests\StaticContentTypeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;


/**
 * Class StaticContentTypeCrudController
 * @package KDA\Backpack\StructuredEditor\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StaticContentTypeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    } //IMPORTANT HERE
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    } //IMPORTANT HERE

    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;
    use \KDA\Backpack\StructuredEditor\Http\Controllers\Traits\GenerateFields;

    public function setup()
    {
        $this->crud->setModel('KDA\Backpack\StructuredEditor\Models\StaticContentType');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/staticcontenttype');
        $this->crud->setEntityNameStrings('staticcontenttype', 'static_content_types');
        $this->loadPermissions('static_content_type');
    }

    protected function setupListOperation()
    {
        kda_no_warranty_alert();
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        CRUD::addColumn('name');
        CRUD::addColumn('description');
        CRUD::addColumn('is_block');
        CRUD::addColumn('is_type');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(StaticContentTypeRequest::class);
        $field_types = $this->getTypes();
        $types = collect($field_types)->map(function ($type, $key) {
            return $type['label'];
        })->toArray();

        CRUD::addField([
            'name' => 'name',
            'type' => 'text',
            'label'=> 'Nom'
        ]);
        CRUD::addField([
            'name' => 'template',
            'label'=> 'Template'

        ]);
        CRUD::addField([
            'name' => 'is_block',
            'label'=> 'Block',

            'type' => 'boolean',
            'hint' => 'Ce type est utilisable pour construire des pages'
        ]);
        CRUD::addField([
            'name' => 'is_type',
            'label'=> 'Type',

            'type' => 'boolean',
            'hint' => 'Ce type est utilisable pour construire des blocks'
        ]);
        CRUD::addField([
            'name' => 'description',
            'label'=> 'Description',

            'type' => 'text',
        ]);

        CRUD::addField([
            'name' => 'slots_list',
            'label'=> 'Slots',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'id',
                    'type'    => 'hidden',
                ],
                [
                    'name' => 'name',
                    'type' => 'text',
                    'label'=> 'Nom'
                ]
            ],
            'new_item_label'  => 'Add Slot',
            'init_rows' => 0,
            'min_rows' => 0,
        ]);
        CRUD::addField(
            [   // repeatable
                'name'  => 'fields',
                'label' => 'Champs',
                'tab' => 'Champs',
                'type'  => 'repeatable',
                'fields' => [
                    [
                        'name'    => 'label',
                        'type'    => 'text',
                        'label'   => 'Label',
                        'hint' => 'Nom du champ pour l\'utilisateur',
                        'wrapper' => ['class' => 'form-group col-md-4'],
                    ],
                    [
                        'name'    => 'name',
                        'type'    => 'text',
                        'label'   => 'Nom',
                        'hint' => 'Nom du champ pour le developpeur, sans espace ni maj',

                        'wrapper' => ['class' => 'form-group col-md-4'],
                    ],
                    [   // select2_from_array
                        'name'        => 'type',
                        'label'       => "Type",
                        'type'        => 'select2_from_array',
                        'options'     => $types,
                        'allows_null' => false,
                        'wrapper' => ['class' => 'form-group col-md-4'],
                        'default'     => 'one',
                    ],
                    [
                        'name'    => 'hint',
                        'type'    => 'text',
                        'label'   => 'Hint',
                        'hint' => 'Description du champ pour l\'utilisateur'
                    ],
                    [
                        'name'    => 'tab',
                        'type'    => 'text',
                        'label'   => 'Tab',
                        'hint' => 'Does nothing in repeat mode',
                        'wrapper' => ['class' => 'form-group col-md-4'],

                    ],
                    [
                        'name'    => 'wrapper',
                        'type'    => 'table',
                        'wrapper' => ['class' => 'form-group col-md-4'],

                        'columns'         => [
                            'key'  => 'Key',
                            'value'  => 'Value'
                        ],
                        'label'   => 'Wrapper',
                        'hint' => 'field wrapper'
                    ],
                    [
                        'name'    => 'options',
                        'type'    => 'table',
                        'wrapper' => ['class' => 'form-group col-md-4'],

                        'columns'         => [
                            'key'  => 'Key',
                            'value'  => 'Value'
                        ],
                        'label'   => 'Options',
                        'hint' => 'liste'
                    ],

                ],

                // optional
                'new_item_label'  => 'Add Field', // customize the text of the button
                'init_rows' => 0,
                   'min_rows' => 0,
                'max_rows' => 0, // maximum rows allowed, when reached the "new item" button will be hidden


            ]
        );


        CRUD::addField([
            'name'     => 'repeatable',
            'label'    => "Répétable",
            'type' => 'toggle',
            'view_namespace' => 'kda-backpack-custom-fields::fields',
            'fake'     => true,
            'tab' => 'Répetition',
            'store_in' => 'repeatable' // [optional]
        ]);
        CRUD::addField([
            'name'     => 'init_rows',
            'label'    => "Nombre initial",
            'type' => 'text',
            'default' => 0,
            'fake'     => true,
            'tab' => 'Répetition',
            'wrapper' => ['class' => 'col-md-4'],
            'store_in' => 'repeatable' // [optional]
        ]);
        CRUD::addField([
            'name'     => 'min_rows',
            'label'    => "Minimum",
            'type' => 'text',
            'default' => 0,
            'fake'     => true,
            'tab' => 'Répetition',
            'wrapper' => ['class' => 'col-md-4'],
            'store_in' => 'repeatable' // [optional]
        ]);
        CRUD::addField([
            'name'     => 'max_rows',
            'label'    => "Maximum",
            'type' => 'text',
            'fake'     => true,
            'tab' => 'Répetition',
            'default' => '0',
            'wrapper' => ['class' => 'col-md-4'],
            'store_in' => 'repeatable' // [optional]
        ]);
        CRUD::addField(
            [   // repeatable
                'name'  => 'layout',
                'label' => 'Champs',
                'tab' => 'Mise en page',
                'type'  => 'repeatable',
                'fields' => [
                    [
                        'name'    => 'label',
                        'type'    => 'text',
                        'label'   => 'Label',
                        'hint' => 'Nom du champ pour l\'utilisateur',
                        'wrapper' => ['class' => 'form-group col-md-4'],
                    ],
                    [
                        'name'    => 'name',
                        'type'    => 'text',
                        'label'   => 'Nom',
                        'hint' => 'Nom du champ pour le developpeur, sans espace ni maj',

                        'wrapper' => ['class' => 'form-group col-md-4'],
                    ],
                    [
                        'name'    => 'values',
                        'type'    => 'table',
                        'wrapper' => ['class' => 'form-group col-md-4'],

                        'columns'         => [
                            'key'  => 'Clé',
                            'value'  => 'Value'
                        ],
                        'label'   => 'Values',
                        'hint' => 'Valeurs'
                    ],

                    [
                        'name' => 'default',
                        'type' => 'text',
                        'label' => 'Valeur par défaut'
                    ],
                    [
                        'name'    => 'hint',
                        'type'    => 'text',
                        'label'   => 'Hint',
                        'hint' => 'Description du champ pour l\'utilisateur'
                    ],

                ],

                // optional
                'new_item_label'  => 'Add Field', // customize the text of the button
                'init_rows' => 0, // number of empty rows to be initialized, by default 1
                'min_rows' => 0, // minimum rows allowed, when reached the "delete" buttons will be hidden
                'max_rows' => 0, // maximum rows allowed, when reached the "new item" button will be hidden


            ]
        );
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store()
    {
        $items = collect(json_decode(request('slots_list'), true));
        // create the main item as usual
        $response = $this->traitStore();

        // instead of returning, take a little time to create the post comments too
        $id = $this->crud->entry->id;


        // add the id to the items collection
        $items->each(function ($item, $key) use ($id) {
            $item['content_type_id'] = $id;
            \KDA\Backpack\StructuredEditor\Models\StaticSlot::create($item);
        });

        return $response;
    }

    public function update()
    {
        $items = collect(json_decode(request('slots_list'), true));

        $response = $this->traitUpdate();

        // instead of returning, take a little time to update the post comments too
        $id = $this->crud->entry->id;
        $created_ids = [];

        $items->each(function ($item, $key) use ($id, &$created_ids) {
            $item['content_type_id'] = $id;

            if ($item['id']) {
                $slot = \KDA\Backpack\StructuredEditor\Models\StaticSlot::find($item['id']);
                $slot->update($item);
            } else {
                $created_ids[] = \KDA\Backpack\StructuredEditor\Models\StaticSlot::create($item)->id;
            }
        });

        // delete removed Comments
        $related_items_in_request = collect(array_merge($items->where('id', '!=', '')->pluck('id')->toArray(), $created_ids));
        $related_items_in_db = $this->crud->entry->slots;

        $related_items_in_db->each(function ($item, $key) use ($related_items_in_request) {
            if (!$related_items_in_request->contains($item['id'])) {
                $item->delete();
            }
        });

        return $response;
    }
}
