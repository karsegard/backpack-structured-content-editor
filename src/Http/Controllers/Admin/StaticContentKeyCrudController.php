<?php

namespace KDA\Backpack\StructuredEditor\Http\Controllers\Admin;

use KDA\Backpack\StructuredEditor\Http\Requests\StaticContentKeyRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class StaticContentKeyCrudController
 * @package KDA\Backpack\StructuredEditor\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StaticContentKeyCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    } //IMPORTANT HERE
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    } //IMPORTANT HERE
    public function setup()
    {
        $this->crud->setModel('KDA\Backpack\StructuredEditor\Models\StaticContentKey');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/staticcontentkey');
        $this->crud->setEntityNameStrings('bloc', 'blocs');
        $this->loadPermissions('static_content_key');
        // $this->setupManageSubControllersOperation();
    }
    /*
    protected function setupManageSubControllersOperation()
    {
        $this->setSubControllers([
            'blockviewcomposer'=>[
                'key'=>'content_key_id',
                'label'=> 'Gérer les composers',
            ]
        ],'staticcontentkey');

        $this->setGetSubEntry(function($id){
            return $this->getEntry($id)->load('content');
        });
        
    }

*/
    protected function setupListOperation()
    {
        kda_no_warranty_alert();
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        CRUD::column('hint')->label('Contenu');
        CRUD::column('contentType')
            ->label('Type de données')
            ->attribute('name')
            ->searchLogic(function ($query, $column, $searchTerm) {
                $query->orWhereHas('contentType', function ($q) use ($column, $searchTerm) {
                    $q->where('name', 'like', '%'.$searchTerm.'%')
                      ->orWhere('description', 'like', '%'.$searchTerm.'%');
                });
            });
        CRUD::column('name')->label('Clé');
        CRUD::column('slotteds')->label('Used by')->type('relationship_count')->attribute('hint');
        CRUD::column('slots')->label('Uses')->type('relationship_count')->attribute('hint');
        CRUD::column('pages')->label('Pages');
        CRUD::column('contents')->type('relationship_count');
        CRUD::column('template')->label('Template');

    }

    protected function setupCreateOperation()
    {
        $this->crud->with('composers');
        $this->crud->setValidation(StaticContentKeyRequest::class);
        CRUD::addField([
            'name' => 'name',
            'type' => 'text',
            'label'=> 'Name'
        ]);
        CRUD::addField([
            'name' => 'hint',
            'type' => 'text',
            'label'=> 'Hint'

        ]);
        CRUD::addField([
            'name' => 'template',
            'label'=> 'Template'

        ]);

        CRUD::addField([
            'name' => 'composers',
            'type' => 'relationship',
            'label'=> 'Composers',
            'model' => "\KDA\Backpack\StructuredEditor\Models\ViewComposer",
        ]);
        CRUD::addField([
            'name' => 'slots_list',
            'label'=> 'Slots',

            'type' => 'repeatable',
            'fields' => [
                [
                    'name' => 'slot_content_key_id',
                    'label' => 'Block',
                    'type' => 'select2',
                    'model' => '\KDA\Backpack\StructuredEditor\Models\StaticContentKey',
                    'attribute' => 'full'
                ],
                [
                    'name' => 'slot_name',
                    'type' => 'text',
                    'label'=> 'Nom'
                ]
            ],
            'init_rows' => 0,
            'min_rows' => 0,
        ]);
        // TODO: remove setFromDb() and manually define Fields
        CRUD::addField([
            'type' => "relationship",
            'name' => 'contentType', // the method on your model that defines the relationship
            'label' => 'Type de donnée', // the method on your model that defines the relationship
            'ajax' => false,
            'attribute' => 'name',
            'wrapper' => ['class' => 'col-md-6'],

            // that way the assumed URL will be "/admin/tag/inline/create"
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }


    public function store()
    {
        $list = request('slots_list');

        if (is_string($list)){
            $list = json_decode($list,true);
        }
        $roles = collect($list);
        $response = $this->traitStore();

        $this->crud->entry->slots()->sync($roles);

        return $response;
    }

    public function update()
    {
        $list = request('slots_list');

        if (is_string($list)){

            $list = json_decode($list,true);
        }

        $roles = collect($list);
        $response = $this->traitUpdate();

        $this->crud->entry->slots()->sync($roles);

        return $response;
    }
}
