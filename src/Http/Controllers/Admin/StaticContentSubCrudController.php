<?php

namespace KDA\Backpack\StructuredEditor\Http\Controllers\Admin;

use KDA\Backpack\StructuredEditor\Http\Requests\StaticContentRequest;
use KDA\Backpack\StructuredEditor\Http\Requests\StaticContentUpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use DB;

class StaticContentSubCrudController extends StaticContentCrudController
{
    use \KDA\Backpack\Subcontroller\Traits\SubCrudController;


    public function setup()
    {
        parent::setup();
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/staticpagesubcontent');
        $this->setParentController([
            'route' => 'staticpagecontent',
            'clause' => 'forPage',
            'key' => 'page_id'
        ]);
        $this->loadPermissions(
            'page',
            ['list', 'preview', 'show', 'edit', 'create', 'update'],
            [
                'read' => [
                    'list',
                    'show',
                    'preview'
                ],
                'write' => [
                    'create',
                    'delete',
                    
                ],
                'content' => [
                    'edit',
                    'update',

                ]


            ]
        );
        $this->setupSubController(['parentIsHandlingList'=>true]);

    }

    protected function setupUpdateOperation()
    {
        $this->crud->setHeading('Edition');
        $this->crud->setSubheading('Modification du contenu');
        $this->crud->setFormHeaderBackString('Retour aux blocs');
        parent::setupUpdateOperation();
        $this->setupSubControllerUpdateOperation();
    }
}
