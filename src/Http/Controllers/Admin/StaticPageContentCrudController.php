<?php

namespace KDA\Backpack\StructuredEditor\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use DB;
use KDA\Backpack\StructuredEditor\Models\StaticContent;
use KDA\Backpack\Subcontroller\Controllers\PluginController;
use KDA\Backpack\StructuredEditor\Models\StaticContentKey;
use Str;

/**
 * Class StaticContentCrudController
 * @package KDA\Backpack\StructuredEditor\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */

class StaticPageContentCrudController extends PluginController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    } //IMPORTANT HERE
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
    use \KDA\Backpack\StructuredEditor\Http\Controllers\Traits\RenderBlock;


    use \KDA\Backpack\Subcontroller\Traits\SubCrudController;
    use \KDA\Backpack\Subcontroller\Operations\ManageSubControllersOperation;

    public function setup()
    {
        $this->crud->setModel('KDA\Backpack\StructuredEditor\Models\StaticPageContent');
        $this->crud->setEntityNameStrings('bloc', 'blocs');

        $this->loadPermissionsDenyFirst(
            'page',
            ['list', 'preview', 'show', 'edit', 'create', 'delete','reorder', 'update','content'],
            [
                'read' => [
                    'list',
                    'show',
                    'preview'
                ],
                'write' => [
                    'delete',
                    'edit',
                    'create',
                    'update',
                    'reorder',
                    'content'
                ],
                'content' => [
                    'content'
                ],
                'manage'=>[
                    'create',
                    'update',
                    'reorder',
                    'content'

                ]


            ]
        );


        $this->setParentController([
            'route' => 'staticpage',
            'clause' => 'forPage',
            'key' => 'page_id'
        ]);
        $this->setupSubController();
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/staticpagecontent');
        $this->setupManageSubControllersOperation();
    }



    protected function setupManageSubControllersOperation()
    {
        $this->setSubControllers([
            'staticpagesubcontent' => [
                'key' => 'page_content_id',
                'label' => 'Editer contenu',
                'subaction' => 'edit',
                'subaction_key' => 'content.id',
                'operation_name'=>'content'
            ]
        ], 'staticpagecontent');

        $this->setGetSubEntry(function ($id) {
            return $this->getEntry($id)->load('content');
        });
    }

    protected function setupListOperation()
    {
        $this->setupSubControllerListOperation();
        $this->crud->enableDetailsRow();
        $this->crud->disablePersistentTable();
        $this->crud->setOperationSetting('sc_back.route', 'staticpage');
        $this->crud->orderBy('lft');
        if (!$this->crud->isSubcontrolled()) {
            CRUD::addColumn([
                'label' => 'Page',
                'name' => 'page',

            ]);
        }
        CRUD::addColumn([
            'name' => 'contentKey',
            'label'=>'Bloc',
            'attribute' => 'hint'
        ]);
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements 
        $this->crud->set('reorder.label', 'contentKey.hint');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 1);
        $this->setupSubControllerReorderOperation();
    }

    protected function showDetailsRow($id)
    {
        return view('preview-block-render', ['block' => $this->crud->getModel()->find($id), 'id' => $id]);
    }

    public function preview($id)
    {
        $entry = $this->crud->getEntry($id);

        return view('preview-block', ['block' => $this->renderBlock($entry)]);
    }

    protected function setupBaseCreateOperation()
    {
        if (!$this->crud->isSubcontrolled()) {
            CRUD::addField([
                'label' => 'Page',
                'name' => 'page',
                'type' => 'relationship'
            ]);
        }
        CRUD::addField([  // Select
            'label'     => "Bloc existant",
            'type'      => 'select2',
            'name'      => 'content_key_id', // the db column for the foreign key
            'value' => NULL,
            'allows_null' => true,
            'placeholder' => 'Select an existing block',
            // optional
            // 'entity' should point to the method that defines the relationship in your Model
            // defining entity will make Backpack guess 'model' and 'attribute'
            'entity'    => 'contentKey',
            'tab' => 'Existing block',

            // optional - manually specify the related model and attribute
            'model'     => "KDA\Backpack\StructuredEditor\Models\StaticContentKey", // related model
            'attribute' => 'hint', // foreign key attribute that is shown to user

            // optional - force the related options to be a custom query, instead of all();
            'options'   => (function ($query) {
                return $query->whereHas('contentType', function ($q) {
                    $q->where('is_block', true);
                })->get();
            }), //  you can use this to filter the results show in the select
        ],);
    }
    protected function setupCreateOperation()
    {
        $this->setupSubControllerCreateOperation();
        /*CRUD::addField([
            'name' => 'contentKey',
            'type'=>'relationship'
        ]);*/

        $this->setupBaseCreateOperation();
        CRUD::addField([  // Select
            'label'     => "Nouveau block de type",
            'type'      => 'select2',
            'name'      => 'content_type_id', // the db column for the foreign key
            'value' => NULL,
            'allows_null' => true,


            // optional - manually specify the related model and attribute
            'model'     => "KDA\Backpack\StructuredEditor\Models\StaticContentType", // related model
            'attribute' => 'description', // foreign key attribute that is shown to user
            'tab' => 'New block',
            // optional - force the related options to be a custom query, instead of all();
            'options'   => (function ($query) {
                return $query->where('is_block', true)->get();
            }), //  you can use this to filter the results show in the select
        ],);
        CRUD::addField([  // Select
            'label'     => "Nom",
            'type'      => 'text',
            'name'      => 'new_block_name', // the db column for the foreign key
            'tab' => 'New block',

        ],);
    }

    protected function setupUpdateOperation()
    {

        $this->setupSubControllerUpdateOperation();
        $entry = $this->crud->getCurrentEntry();

        $contentType = $entry->contentKey->contentType;


        $fields = [];
        foreach ($contentType->layout as $field) {
            $values = json_decode($field['values'], true);
            $values = collect($values)->mapWithKeys(function ($item, $value) {
                return [$item['key'] => $item['value']];
            })->toArray();
            $value = $entry->layout[$field['name']] ?? $field['default'] ?? NULL;
            $fields[] = array_merge([
                'name' => $field['name'],
                'label' => $field['label'] ?? $field['name'],
                'type' => 'select_from_array',
                'options' => $values,
                'value' => $value,
                'wrapper' => (!empty($field['wrapper'])) ? $field['wrapper'] : [],
                'tab' => (!empty($field['tab'])) ? $field['tab'] : 'Contenu',
                'hint' => $field['hint'],

            ]);
        }

        collect($fields)->map(function ($f) {

            $f['store_in'] = 'layout';
            $f['fake'] = true;
            CRUD::addField($f);
        });
    }

    public function store()
    {
        $request = $this->crud->getStrippedSaveRequest();

        if (!$request['content_key_id']) {
            $name = $request['new_block_name'];
            $type = $request['content_type_id'];
            if ($name && $type) {
                $slug = Str::slug($name, '_');
            }

            $id = (StaticContentKey::create([
                'name' => $slug,
                'content_type_id' => $type,
                'hint' => $name,
                'is_block' => true
            ]))->id;

            $this->crud->getRequest()->request->set('content_key_id', $id);

            $request['content_key_id'] = $id;
        }

        $contentKey = StaticContentKey::find($request['content_key_id']);

        $has_content = StaticContent::current($contentKey->name);
        if (!$has_content) {
            StaticContent::create([
                'content_key_id' => $request['content_key_id']
            ]);
        }

        $response = $this->traitStore();




        return $response;
    }
}
