<?php

namespace  KDA\Backpack\StructuredEditor\Http\Controllers;

use Illuminate\Routing\Controller;
use KDA\Backpack\StructuredEditor\Models\StaticPage;
use Illuminate\Support\Facades\View;
use KDA\Backpack\StructuredEditor\Http\Controllers\Traits\RenderBlock;

use KDA\Sluggable\Facades\Slug;

class StaticContentController extends Controller
{
    use RenderBlock;
   
    public function byId($id){

        return  $this->renderPage($id);
    }

    
    public function bySlug($slug){

        $page = Slug::getFirstByCollection('pages',$slug);
        return  $this->renderPage($page->id);
    }

}
