<?php

namespace  KDA\Backpack\StructuredEditor\Http\Controllers\Traits;

use KDA\Backpack\StructuredEditor\Models\StaticPage;
use KDA\Backpack\StructuredEditor\Models\StaticContent;
use Illuminate\Support\Facades\View;
use KDA\Backpack\StructuredEditor\Facades\StructuredContent;

trait RenderBlock
{

    public function renderPage($id)
    {
        $page = StaticPage::find($id);
        $views=[];
        foreach ($page->blocks as $block) {

            $views[] = $this->renderBlock($block);
        }

        return view($page->template, ['views' => $views,'page'=>$page]);
    }


    public function renderContent($content)
    {

        $data = sc_get_content($content->contentKey->name) ?? [];

        if ($content->contentKey->contentType->is_repeatable) {
           return  $this->renderRepeatableContent($content, $data);
        }
        return $this->renderSingleContent($content,$data);
    }

    public function renderSingleContent($content, $data)
    {
        $types = $content->contentKey->contentType->all_fields_with_types;

        foreach ($data as $key => $d) {
            $type = $types->get($key);
            if ($type) {
                $data[$key] = view('sc::render-field', [
                    'field' => $d,
                    'type' => $type
                ]);
            }
        }
        return $data;
    }

    public function renderRepeatableContent($content, $data)
    {
        $types = $content->contentKey->contentType->all_fields_with_types;

        foreach ($data as $id => $item) {
            foreach ($item as $key => $d) {
                $type = $types->get($key);
                if ($type) {
                    $data[$id][$key] = view('sc::render-field', [
                        'field' => $d,
                        'type' => $type
                    ]);
                }
            }
        }
        return $data;
    }

    public function renderBlock($block)
    {
        $content = StructuredContent::getCurrentContentForKeyId($block->content_key_id);

        $template = $block->template;

        $layout = $block->layoutData;

        
        $data = sc_get_content($content->contentKey->name) ?? [];

        $slots = $content->contentKey->slots;
        $slots_data = [];
        foreach ($slots as $slot) {
            $slot_name = $slot->pivot->slot_name;
            $slotContent = StaticContent::where('content_key_id', $slot->id)->current()->first();
            $render = $this->renderContent($slotContent);
            if($slot->contentType->is_repeatable){
                $render = ['value'=>$render];
            }
            $slots_data[$slot_name] = view($slot->template,$render);
        }

        $types = $content->contentKey->contentType->all_fields_with_types;

        foreach ($data as $key => $d) {
            $type = $types->get($key);
            if ($type) {
                $data[$key] = view('sc::render-field', [
                    'field' => $d,
                    'type' => $type
                ]);
            }
        }

        $vars = array_merge($layout, $data,$slots_data);

        $vars['content'] = $content;
        if ($template) {

            if ($block->view_composers) {
                foreach ($block->view_composers as $composer) {
                    View::composer($template, $composer->name);
                }
            }

            return  view($template, $vars);
        } else {
            return 'Template not defined for ' . $content->contentKey->name . ' of type ' . $content->contentKey->contentType->name;
        }
    }
}
