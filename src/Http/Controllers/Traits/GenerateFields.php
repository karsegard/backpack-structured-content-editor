<?php

namespace  KDA\Backpack\StructuredEditor\Http\Controllers\Traits;

use KDA\Backpack\StructuredEditor\Facades\StructuredContent;
use KDA\Backpack\StructuredEditor\Models\StaticContent;
use KDA\Backpack\StructuredEditor\Models\StaticPage;
use KDA\Backpack\StructuredEditor\Models\StaticContentType;

trait GenerateFields
{

    public function getTypes()
    {
        return StructuredContent::getTypes();
    }

    public function generateField($field, $tab = 'Contenu')
    {
        $ns = $field_types[$field['type']]['view_namespace'] ?? '';
        $namespace = [];
        if ($ns) {
            $namespace['view_namespace'] = $ns;
        }
        $options = $field_types[$field['type']]['options'] ?? [];
        return  array_merge(
            [
                'name' => $field['name'],
                'label' => $field['label'] ?? $field['name'],
                'type' => $field['type'],
                'wrapper' => (!empty($field['wrapper'])) ? $field['wrapper'] : [],
                'tab' => (!empty($field['tab'])) ? $field['tab'] : $tab,
                'hint' => $field['hint'],

            ],
            $namespace,
            $options
        );
    }


    public function generateFields($contentType, $name = NULL, $label = NULL, $tab = 'Contenu',$values=[])
    {
        $repeatable = $contentType->repeatable;
        $field_types = $this->getTypes();


        $is_repeatable = $repeatable['repeatable'] != "0";
        $fields = [];
        $definition = collect($contentType->fields_with_types);
        foreach ($definition->where('__type.key', '') as $field) {
            $ns = $field_types[$field['type']]['view_namespace'] ?? '';
            $namespace = [];
            $value = [];
            $settedValue = $values[$field['name']]??NULL;
            if($settedValue){
                $value['value']= $values[$field['name']];

            }
            if ($ns) {
                $namespace['view_namespace'] = $ns;
            }
            $options = $field_types[$field['type']]['options'] ?? [];
            $__options = $field['options'] ?? "";
            $__options_values = collect(json_decode($__options, true))->mapWithKeys(function ($item) {
                return [$item['key'] => $item['value']];
            });
            $fields[] = array_merge(
                [
                    'name' => $name ? $name . '_' . $field['name'] : $field['name'],
                    'label' => $label ? '(' . $label . ') ' . $field['label'] : $field['label'],
                    'type' => $field['type'],
                    'options' => $__options_values,
                    'wrapper' => (!empty($field['wrapper'])) ? $field['wrapper'] : [],
                    'tab' => (!empty($field['tab'])) ? $field['tab'] : $tab,
                    'hint' => $field['hint'],
                    

                ],
                $value,
                $namespace,
                $options
            );
        }

        foreach ($definition->where('__type.key', '!=', '') as $field) {

            $fieldKey = StaticContentType::where('name', $field['__type']['key'])->first();
            $name = $field['name'];
            $label = $field['label'];
            $fields = array_merge($fields, $this->generateFields($fieldKey, $name, $label));
        }
        //  dump($fields);


        return $fields;
    }


    public function generateSlots($contentKey)
    {
        $slots = [];
        foreach ($contentKey->slots as $slot) {
            $slot_name = $slot->pivot->slot_name;

            //  dump($slot);
           // $slotContent = StaticContent::where('content_key_id', $slot->id)->current()->first();
            $slotContent = StructuredContent::getCurrentContentForKeyId($slot->id);

            if ($slot->contentType->is_repeatable) {

                $slots[$slot_name] = $this->generateFields($slot->contentType, NULL, NULL, $slot->hint);

                $slots[$slot_name] = $this->createRepeatable(
                    $slots[$slot_name],
                    'slot_' . $slot_name,
                    $slotContent->value,
                    $slot->hint,
                    [],
                    $slot->hint
                );
            }else {
                $slots[$slot_name] = $this->generateFields($slot->contentType, $slot_name, NULL, $slot->hint,$slotContent->value);

            }
        }
        return $slots;
    }


    public function createRepeatable($fields, $name, $value = '', $label, $opts = [], $tab = '')
    {
        return [[
            'type' => "repeatable",
            'name' => $name,
            'label' => $label,
            'tab' => $tab,
            'fields' =>  $fields,
            'value' => $value

        ]];
    }
}
