<?php 
namespace  KDA\Backpack\StructuredEditor\Facades;

use Illuminate\Support\Facades\Facade;


class StructuredContent extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'scm-library'; }
}