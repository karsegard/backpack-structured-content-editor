<?php

namespace KDA\Backpack\StructuredEditor;


use KDA\Laravel\PackageServiceProvider;
use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{

    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasHelper;
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasComponents;
    use \KDA\Laravel\Traits\HasLoadableMigration;
    use \KDA\Laravel\Traits\HasRoutes;

    protected $viewNamespace = 'sc';
    protected $publishViewsTo = 'vendor/sc';

    protected $components = [
        'sc-render'=> View\Components\Render::class,
        'sc-render-block'=> View\Components\RenderBlock::class,
        'sc-render-field'=> View\Components\RenderField::class,
        'sc-field-editorjs'=> View\Components\FieldEditorjs::class,
        'sc-field-text'=> View\Components\FieldText::class,
        'sc-field-browse'=> View\Components\FieldBrowse::class,
        'sc-field-select2_from_array'=> View\Components\FieldSelect::class,

        'sc-ejs-block'=> View\Components\EditorJS\Block::class,
        'sc-ejs-image'=> View\Components\EditorJS\Image::class,
        'sc-ejs-header'=> View\Components\EditorJS\Header::class,
        'sc-ejs-nested'=> View\Components\EditorJS\Nested::class,
        'sc-ejs-list'=> View\Components\EditorJS\Listing::class,
        'sc-ejs-paragraph'=> View\Components\EditorJS\Paragraph::class,
        'sc-ejs-table'=> View\Components\EditorJS\Table::class,
        'sc-ejs-render'=> View\Components\EditorJS\Render::class,
    ];

    protected $_commands = [
      
        Commands\InstallCommand::class,
    ];

    protected $configs= [
        'kda/backpack/structured_editor.php'  =>'kda.backpack.structured_editor'
    ];

    protected $routes = [
        'backpack/structured_editor.php'
    ];


    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    public function register(){
        parent::register();
        $this->app->singleton('scm-library',function($q){
            return new Library\StructuredContent();
        });
    }

    protected function bootSelf(){
        Blade::directive('bind_values', function ($content) {
            return "<?php 
                foreach (\$content  as \$field => \$value) {
                    \$\$field = \$value;
                }
    
            ?>";
        });
    }


   

}
