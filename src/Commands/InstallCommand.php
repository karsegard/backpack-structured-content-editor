<?php

namespace KDA\Backpack\StructuredEditor\Commands;

use GKA\Noctis\Providers\AuthProvider;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;

class InstallCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'kda:sceditor:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fresh migration';


    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }


    public function fire()
    {
        return $this->handle();
    }
    protected function getOptions()
    {
        return [
            ['stage', 's', InputOption::VALUE_REQUIRED, 'force stage'],
        ];
    }
    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
      /* if(class_exists("KDA\\Backpack\\DynamicSidebar\\ServiceProvider")){

        $this->info('installing sidebar items');
        \KDA\Backpack\DynamicSidebar\Models\Sidebar::create([
            'label'=>'Types de contenu',
            'route'=>'staticcontentkey',
            'icon'=> 'la-wrench'
        ]);
        \KDA\Backpack\DynamicSidebar\Models\Sidebar::create([
            'label'=>'Contenu Statique',
            'route'=>'staticcontent',
            'icon'=> 'la-pen-nib'

        ]);
        \KDA\Backpack\DynamicSidebar\Models\Sidebar::create([
            'label'=>'Types de Champs',
            'route'=>'staticcontenttype',
            'icon'=> 'la-screwdriver'
        ]);
       }*/
    }
}
