<?php

namespace KDA\Backpack\StructuredEditor\View\Components;

use Illuminate\View\Component;

class FieldText extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $content ;
    public function __construct($content)
    {
        //
        $this->content = $content;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view(sc_config('views.field-text'));
    }
}
