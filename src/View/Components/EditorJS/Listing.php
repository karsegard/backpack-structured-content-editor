<?php

namespace KDA\Backpack\StructuredEditor\View\Components\EditorJS;

use Illuminate\View\Component;

class Listing extends Block
{
  
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view(sc_config('views.ejs-block-list'));

    }
}
