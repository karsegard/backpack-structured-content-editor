<?php

namespace KDA\Backpack\StructuredEditor\View\Components\EditorJS;

use Illuminate\View\Component;

class Render extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $blocks;
    public function __construct($blocks=[])
    {
        //
        $this->blocks = $blocks;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view(sc_config('views.ejs-block-render'));
    }
}
