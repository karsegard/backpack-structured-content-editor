<?php

namespace KDA\Backpack\StructuredEditor\View\Components;

use Illuminate\View\Component;

class Render extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $block;
    public function __construct($block=NULL)
    {
        //
        $this->block = $block;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view(sc_config('views.render'));
    }
}
