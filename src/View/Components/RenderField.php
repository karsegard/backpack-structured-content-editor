<?php

namespace KDA\Backpack\StructuredEditor\View\Components;

use Illuminate\View\Component;

class RenderField extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $field;
    public $type;
    public $name;
    public function __construct($field,$name="",$type="")
    {
        //
        $this->field = $field;
        $this->type=  $type;
        $this->name=  $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view(sc_config('views.render-field'));
    }
}
