<?php

namespace KDA\Backpack\StructuredEditor\View\Components;

use Illuminate\View\Component;

class FieldEditorjs extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $content ;
    public function __construct($content)
    {
        //
        $this->content = json_decode($content,true);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view(sc_config('views.field-editorjs'));
    }
}
