<?php

namespace KDA\Backpack\StructuredEditor\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class StaticContent extends Model
{
        use CrudTrait;

        use \KDA\Laravel\Models\Traits\Schedulable;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                'content_key_id',
                'value',
                'start_on',
                'end_on',
                'enabled',
        ];

        protected $casts = [
                'id' => 'integer',
                'content_key_id' => 'integer',
                'start_on' => 'date',
                'end_on' => 'date',
                'enabled' => 'boolean',
                'value' => 'array',
        ];
        protected $fakeColumns = [
                'value',

        ];

        protected $schedulable = [
                'start' => 'start_on',
                'end' => 'end_on',
                'enabled' => 'enabled'
        ];
        public function contentKey()
        {
                return $this->belongsTo(StaticContentKey::class);
        }
        public function pages()
        {
                return $this->hasManyThrough(StaticPage::class, StaticPageContent::class, 'content_key_id', 'id', 'content_key_id', 'page_id');
                //3 -> local id

        }

        public function schedulableGroup($q, $key)
        {
                return $q->whereHas('contentKey', function ($q) use ($key) {
                        $q->where('name', $key);
                });
        }

        public function getSchedulableGroupAttribute()
        {
                return $this->contentKey->name;
        }

        public function getTypesAttribute()
        {
                return $this->contentKey->contentType->fields_by_type;
        }

        public function getTemplateAttribute()
        {
                $template =  $this->attributes['template'] ?? $this->contentKey->template;
                return $template;
        }

        public function getLayoutAttribute()
        {
                return $this->attributes['layout'] ?? $this->contentKey->layout;
        }
        public function getViewComposersAttribute()
        {

                $composers = $this->attributes['composers'] ?? $this->contentKey->view_composers;
                return $composers;
        }
}
