<?php

namespace KDA\Backpack\StructuredEditor\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class StaticContentKey extends Model
{
    use CrudTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'content_type_id',
        'hint',
        'template',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'content_type_id' => 'integer',
    ];

    public function contentType()
    {
        return $this->belongsTo(StaticContentType::class);
    }

    public function pages(){
        return $this->hasManyThrough(StaticPage::class,StaticPageContent::class,'content_key_id','id','id','page_id');
        //3 -> local id
        
    }

    public function composers(){
        return $this->belongsToMany(
            ViewComposer::class,
            'static_content_key_view_composer',
            'content_key_id',
            'composer_id'
            
        );
    }
    
    public function slots(){
        return $this->belongsToMany(
            StaticContentKey::class,
            'static_content_key_slots',
            'content_key_id',
            'slot_content_key_id'
            
        )->withPivot('slot_name');
    }

    public function slotteds(){
        return $this->belongsToMany(
            StaticContentKey::class,
            'static_content_key_slots',
            'slot_content_key_id',
            'content_key_id'
            
        )->withPivot('slot_name');;
    }

    public function contents(){
        return $this->hasMany(
            StaticContent::class,
            'content_key_id'
            
        );
    }

    public function getSlotsListAttribute() {
        return json_encode($this->slots->map(function($slot) {
            return ['slot_content_key_id' => $slot->id, 'slot_name' => $slot->pivot->slot_name];
        }));
    }

    public function getTemplateAttribute()
    {
        $template =  $this->attributes['template'] ;
        if(!$template && $this->contentType) {
            $template = $this->contentType->template;
        }
        return $template;
    }

    public function getLayoutAttribute()
    {
        return $this->attributes['layout'] ?? $this->contentType->default_layout;
    }
    public function getViewComposersAttribute()
    {
        $composers = $this->composers ?? NULL;
        return $composers;
    }
    public function getFullAttribute(){
        return $this->hint.' (type: '.$this->name.')';
    }
}
