<?php

namespace KDA\Backpack\StructuredEditor\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use KDA\Backpack\StructuredEditor\BaseModels as GeneratedModels;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class StaticPageContent extends Model
{
        use CrudTrait;
      
        protected $fillable =[
                'content_key_id',
                'page_id',
                'layout'
        ];
        protected $casts=[
                'layout'=>'array'
        ];
        protected $fakeColumns = [
                'layout',

        ];


        public function scopeForPage($query, $id)
        {
                return $query->where('page_id', $id);
        }


        public function contentKey()
        {
                return $this->belongsTo(StaticContentKey::class);
        }


       

        public function content()
        {
                return $this->hasOne(
                        StaticContent::class, 
                        'content_key_id',
                        'content_key_id'
                )->ofMany([],function($q){
                        return $q->current();
                });
        }

        public function page()
        {
                return $this->belongsTo(StaticPage::class,'page_id');
        }

        public function getTemplateAttribute(){
                $template =  $this->attributes['template'] ?? $this->content->template;
                return $template;
        }
        public function getLayoutDataAttribute(){
                return $this->layout ?? $this->content->layout;
        }

        public function getViewComposersAttribute(){
                $composers = $this->attributes['composers'] ?? $this->content->view_composers;
                return $composers;
        }
        
}
