<?php

namespace KDA\Backpack\StructuredEditor\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

use KDA\Sluggable\Models\Traits\HasSlugs ;

class StaticPage extends Model
{
    use CrudTrait;
    use HasSlugs;

    protected $fillable = [
        'name',
        'template',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];
    public function slugCollectionName(){
        return 'Pages';
    }

    public function getSluggableAttribute(){
        return $this->name;
    }
    public function blocks()
    {
        return $this->hasMany(StaticPageContent::class, 'page_id')->orderBy('lft');
    }

    public function getUrlAttribute(){
        return '/'.$this->slugs->first()->getFullPath();
    }
}
