<?php

namespace KDA\Backpack\StructuredEditor\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;


class BlockViewComposer extends Model
{
        use CrudTrait;
    
    protected $table = 'static_content_key_view_composer';
    protected $fillable = [
        'content_key_id',
        'composer_id'
    ];

    
    public function contentKey(){
        return $this->hasOne(StaticContentKey::class,'content_key_id','id');
    }
    public function composer(){
        return $this->hasOne(ViewComposer::class,'composer','id');
    }
}

