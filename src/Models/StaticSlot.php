<?php

namespace KDA\Backpack\StructuredEditor\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;


class StaticSlot extends Model
{
    use CrudTrait;
    public $timestamps = false;
    protected $table = 'static_content_slots';
    protected $fillable = [
        'content_type_id',
        'name'
    ];
}
