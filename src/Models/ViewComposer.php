<?php

namespace KDA\Backpack\StructuredEditor\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;


class ViewComposer extends Model
{
        use CrudTrait;
    
    protected $fillable = [
        'name',
        'description'
    ];

    public function contentKeys(){
        return 
        $this->belongsToMany(
            StaticContentKey::class,
            'static_content_key_view_composer',
            'composer_id',
            'content_key_id',
            'id',
            'id'
        );
    }
}

