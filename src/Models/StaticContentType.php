<?php

namespace KDA\Backpack\StructuredEditor\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use KDA\Backpack\StructuredEditor\BaseModels as GeneratedModels;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;


class StaticContentType extends Model
{
        use CrudTrait;

        protected $fillable = [
                'name',
                'description',
                'fields',
                'repeatable',
                'layout',
                'is_block',
                'is_type',
                'template',

        ];

        /**
         * The attributes that should be cast to native types.
         *
         * @var array
         */
        protected $casts = [
                'id' => 'integer',
                'is_block' => 'boolean',
                'is_type' => 'boolean',
                'repeatable' => 'array',
                'fields' => 'array',
                'layout' => 'array',
        ];


        protected $fakeColumns = [
                'repeatable'
        ];


        public function slots()
        {
                return $this->hasMany(StaticSlot::class, 'content_type_id', 'id');
        }
        //acessor to get the comments populated in the repeatable field again
        public function getSlotsListAttribute()
        {
                return json_encode($this->slots);
        }

        public function getFieldsByTypeAttribute()
        {
                return collect($this->fields)->pluck('type', 'name');
        }

        public function getDefaultLayoutAttribute()
        {
                $value = json_decode($this->attributes['layout'], true);
                $value = collect($value)->mapWithKeys(function ($i) {
                        return [$i['name'] => $i['default'] ?? ''];
                })->toArray();
                return $value;
        }

        public function getIsRepeatableAttribute(){
                $repeatable = $this->repeatable['repeatable'] ?? 0;
                return $repeatable!=0;
        }

        public function getFieldsWithTypesAttribute()
        {
                $types = app()->make('scm-library')->getTypes();
                return collect($this->fields)->map(function ($i) use ($types) {
                        $i['__type'] = $types[$i['type']];
                        return $i;
                });
        }

        public function getAllFieldsWithTypesAttribute()
        {
                $types = app()->make('scm-library')->getTypes();
                return collect(collect($this->fields)->reduce(function ($carry, $i, $type_key) use ($types) {
                        $type = $i['type'];

                        $t = $types[$i['type']];
                        $key = $t['key'] ?? NULL;
                        //   dump($i);
                        if ($key) {
                                $t = self::where('name', $key)->first();
                                $sub = $t->all_fields_with_types;
                                foreach ($sub as $field_name => $subtype) {
                                        $carry[$i['name'] . '_' . $field_name] = $subtype;
                                }
                        } else {
                                $carry[$i['name']] = $type;
                        }
                        return $carry;
                }, []));
        }
}
