<?php
namespace  KDA\Backpack\StructuredEditor\Library;

use KDA\Backpack\StructuredEditor\Models\StaticPage;
use KDA\Backpack\StructuredEditor\Models\StaticContent;

use KDA\Backpack\StructuredEditor\Models\StaticContentType;
class StructuredContent{

    public function getTypes(){
        
        return array_merge(
            $this->getRawTypes(),
            $this->getRawTypesFromDb()
        );
    }
    public function getRawTypes(){
        
        return config('kda.backpack.structured_editor.field_types');
    }
    public function getRawTypesFromDb(){
        $types = StaticContentType::where('is_type',true)->get();
        $types=  $types->mapWithKeys(function($q){
            return [$q['name']=>[
                'label'=>$q['description'],
                'key'=> $q['name']
            ]];
        })->toArray();
        return $types;
    }

    public function getCurrentContentForKeyId($id,$auto_create=true){
        $content =StaticContent::where('content_key_id', $id)->current()->first();
        if($auto_create && !$content){
            $content = new StaticContent();
            $content->content_key_id = $id;
            $content->save();
        }
        return $content;
    }
}