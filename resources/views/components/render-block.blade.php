@foreach($block as $key => $field)
    <x-sc-render-field :name="$key" :field="$field" :type="$types->get($key)"/>
@endforeach 