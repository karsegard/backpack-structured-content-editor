@php
    $style = $block['data']['style']?? 'unordered';
    //$tag = $style =='ordered' ? 'ol' : 'ul';
@endphp
<ul class="{{$style}}">
    @foreach($block['data']['items'] as $item)
    <li>{!! $item !!}</li>
    @endforeach

</ul>
