@if (is_array($blocks))
    @foreach ($blocks['blocks'] as $block)

        @php
            $component = 'sc-ejs-' . $block['type'];
        @endphp

        <x-dynamic-component :component="$component" :block="$block" />

    @endforeach
@endif
