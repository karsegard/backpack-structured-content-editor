@props([
  
    'block'=>[],
    'offset'=>0
])

@php

    $text = $block['data']['text'] ?? $slot;
    $level = $block['data']['level'] ?? 2;

    $level += $offset;

@endphp
@if(!empty($text))
<h{{$level}}>{{$text}}</h{{$level}}> 
@endif