@php

    $re = '/>([^>]+)</m';
    $str = $block['data']['caption'];

    $subst = '><img src="'.$block['data']['url'].'"/><';

    $result = preg_replace($re, $subst, $str);
    if(empty($result) || strpos($result,'<')===false){

        $result = '<img src="'.$block['data']['url'].'"/>';
    }

@endphp


{!! $result !!}
