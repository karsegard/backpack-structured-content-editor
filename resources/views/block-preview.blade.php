<style>
   .container {  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
  grid-template-rows: auto auto auto;
  grid-auto-columns: 1fr;
  gap: 3px 4px;
  grid-auto-flow: row;
  grid-template-areas:
    ". . . . ."
    ". Headline text text2 ."
    ". . . . .";
}

.Headline { grid-area: Headline; }

.text { grid-area: text; }

.text2 { grid-area: text2; }


</style>
