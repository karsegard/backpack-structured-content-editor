@if ($crud->hasAccess('previewstructuredcontent'))
	<a href="{{ url($crud->route.'/'.$entry->getKey().'/previewstructuredcontent') }}" class="btn btn-sm btn-link" data-style="zoom-in"><span class="ladda-label"><i class="la la-lock"></i>Preview</span></a>
@endif