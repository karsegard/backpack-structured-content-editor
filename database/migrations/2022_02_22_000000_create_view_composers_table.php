<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewComposersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('view_composers', function (Blueprint $table) {
            $table->id();
            $table->string('description');
            $table->string('name');
            $table->timestamps();
        });


        Schema::create('static_content_key_view_composer', function (Blueprint $table) {
            $table->foreignId('content_key_id')->constrained('static_content_keys')->onDelete('cascade');
            $table->foreignId('composer_id')->constrained('view_composers')->onDelete('cascade');
        });

        Schema::create('static_content_key_slots', function (Blueprint $table) {
            $table->foreignId('content_key_id')->constrained('static_content_keys')->onDelete('cascade');
            $table->foreignId('slot_content_key_id')->constrained('static_content_keys')->onDelete('cascade');
            $table->string('slot_name');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('view_composers');
        Schema::dropIfExists('static_content_key_view_composer');
        Schema::dropIfExists('static_content_key_slots');
        Schema::enableForeignKeyConstraints();
    }
}
