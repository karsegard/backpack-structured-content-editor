<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('static_contents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('content_key_id')->constrained('static_content_keys')->onDelete('restrict');
            $table->text('value')->nullable();
            $table->date('start_on')->nullable();
            $table->date('end_on')->nullable();
            $table->boolean('enabled')->default(1);
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_contents');
    }
}
