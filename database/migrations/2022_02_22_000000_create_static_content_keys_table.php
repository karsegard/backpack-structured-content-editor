<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticContentKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('static_content_keys', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('content_type_id')->nullable()->constrained('static_content_types')->onDelete('restrict');
            $table->string('hint')->nullable();
            $table->string('template')->nullable();
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_content_keys');
    }
}
