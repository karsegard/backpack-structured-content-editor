<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticPageContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('static_page_contents', function (Blueprint $table) {
            $table->id();
           
            $table->foreignId('page_id')->constrained('static_pages')->onDelete('restrict');
            $table->foreignId('content_key_id')->constrained('static_content_keys')->onDelete('restrict');
            $table->text('layout')->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->unsignedInteger('lft')->nullable()->default(0);
            $table->unsignedInteger('rgt')->nullable()->default(0);
            $table->unsignedInteger('depth')->nullable()->default(0);
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('static_page_contents');
        Schema::enableForeignKeyConstraints();

    }
}
