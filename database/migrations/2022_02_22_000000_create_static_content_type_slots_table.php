<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticContentTypeSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('static_content_slots', function (Blueprint $table) {
            $table->id();
            $table->foreignId('content_type_id')->constrained('static_content_types')->onDelete('cascade');
            $table->string('name');
        });


        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('static_content_slots');
        Schema::enableForeignKeyConstraints();
    }
}
