<?php

/*
|--------------------------------------------------------------------------
| Backpack\PermissionManager Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are
| handled by the Backpack\PermissionManager package.
|
*/

Route::group([
    'namespace'  => 'KDA\Backpack\StructuredEditor\Http\Controllers\Admin',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware()],
], function () {
    Route::crud('staticcontent', 'StaticContentCrudController');
    Route::crud('staticcontenttype', 'StaticContentTypeCrudController');
    Route::crud('staticcontentkey', 'StaticContentKeyCrudController');
    Route::crud('staticpage', 'StaticPageCrudController');
    Route::crud('staticpagecontent', 'StaticPageContentCrudController');
    Route::crud('staticpagesubcontent', 'StaticContentSubCrudController');
    Route::crud('viewcomposer', 'ViewComposerCrudController');

    Route::get('preview/{id}','StaticPageContentCrudController@preview');
});
